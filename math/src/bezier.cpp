#include "../include/bezier.hpp"

double Bernstein_Polynomial(const double &t, const int &i, const int &n)
{
    return Binomial_theorem(t,1-t,i,n);
}

Point Bezier_order_2(const double &t, const Point &A, const Point &B, const Point &C)
{
    return Bernstein_Polynomial(t,0,2)*A + 
           Bernstein_Polynomial(t,1,2)*B + 
           Bernstein_Polynomial(t,2,2)*C;
}