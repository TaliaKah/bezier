#include "../include/is_in_a_shape.hpp"
#include <math.h>

bool is_in_circle(Point point, Point center, float radius_center){
    Point point_intermediate = (point - center) * (point - center); 
    float radius_point = sqrt(point_intermediate.get_x() + point_intermediate.get_y());
    return (radius_point <= radius_center);
}