#include "../include/point.hpp"
#include <cassert>

Point::Point()
    : _x(0),_y(0) 
{}

Point::Point(const double &x, const double &y)
    : _x(x),_y(y) 
{}

double Point::get_x() const 
{
    return _x;
}

double Point::get_y() const 
{
    return _y;
}

bool Point::is_null () const
{
    return ((_x == 0) && (_y == 0));
}

bool Point::one_coordinate_is_null() const 
{
    return ((_x == 0) || (_y == 0));
}

Point &Point::operator=(const Point &p) 
{
    _x = p.get_x();
    _y = p.get_y();
    return *this;
}

Point operator+(const Point &p1, const Point &p2)
{
    Point point(p1.get_x() + p2.get_x(),
                p1.get_y() + p2.get_y());
    return point;
}

Point &Point::operator+=(const Point &p)
{
    _x+= p.get_x();
    _y+= p.get_y();
    return *this;
}

Point operator-(const Point &p1, const Point &p2)
{
    Point point(p1.get_x() - p2.get_x(),
                p1.get_y() - p2.get_y());
    return point;
}

Point &Point::operator-=(const Point &p)
{
    _x-= p.get_x();
    _y-= p.get_y();
    return *this;
}

Point operator*(const Point &p1, const Point &p2)
{
    Point point(p1.get_x() * p2.get_x(),
                p1.get_y() * p2.get_y());
    return point;
}

Point &Point::operator*=(const Point &p)
{
    _x*= p.get_x();
    _y*= p.get_y();
    return *this;
}

Point operator*(const Point &p, const double &a)
{
    Point point(a * p.get_x(), 
                a * p.get_y());
    return point;
}

Point operator*(const double &a, const Point &p)
{
    Point point(a * p.get_x(), 
                a * p.get_y());
    return point;
}

Point &Point::operator*=(const double &a)
{
    _x*= a;
    _y*= a;
    return *this;
}

Point operator/(const Point &p1, const Point &p2)
{
    assert(!(p2.one_coordinate_is_null()) && "You can't divide a Point coordinate by 0");
    Point point(p1.get_x() / p2.get_x(),p1.get_y() / p2.get_y());
    return point;
}

Point &Point::operator/=(const Point &p)
{
    assert(!(p.one_coordinate_is_null()) && "You can't divide a Point coordinate by 0");
    _x/=p.get_x();
    _y/=p.get_y();
    return *this;
}

Point operator/(const Point &p, const double &a)
{
    assert(!(a == 0) && "You can't divide a Point by 0");
    Point point(p.get_x() / a,p.get_y() / a);
    return point;
}

Point &Point::operator/=(const double &a)
{
    assert(!(a == 0) && "You can't divide a Point by 0");
    _x/=a;
    _y/=a;
    return *this;
}

std::ostream& operator<< (std::ostream& stream, const Point& p)
{
    stream<<"("<<p.get_x()<<","<<p.get_y()<<")\n";
    return stream;
};