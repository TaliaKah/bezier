#include "../include/mathematical_functions.hpp"
#include <cassert>

double Factorial(const int &n)
{
    assert((n >= 0) && "You can't give a negative number to Factorial");
    if (n == 0) return 1;
    else return n*Factorial(n-1);
}

double Power(const double &a, const int &n)
{
    assert((n >= 0) && "You can't give a negative number to Power");
    if (n == 0) return 1;
    else if (n == 1) return a;
    else{
        if (n%2 == 0) return Power(a, n/2)*Power(a, n/2);
        else return Power(a, (n-1)/2)*Power(a, (n+1)/2);
    }
}