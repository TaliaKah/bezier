#include "../include/binomial_theorem.hpp"
#include "../include/mathematical_functions.hpp"

double Binomial_coefficient(const int &k, const int &n)
{
    return Factorial(n)/(Factorial(n-k)*Factorial(k));
}

double Binomial_theorem(const double &a, const double &b, const int &k, const int &n)
{
    return Binomial_coefficient(k,n)*Power(a,k)*Power(b,n-k);
}
