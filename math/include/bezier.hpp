#pragma once
#include "../include/binomial_theorem.hpp"
#include "../include/point.hpp"

double Bernstein_Polynomial(const double &t, const int &i, const int &n);

Point Bezier_order_2(const double &t, const Point &A, const Point &B, const Point &C);