#pragma once

#include <ostream>

class Point
{
    double _x;
    double _y;
    public:
    Point();
    ~Point()=default;
    Point(const double &x, const double &y);


    double get_x() const;
    double get_y() const;

    bool is_null() const;
    bool one_coordinate_is_null() const;

    Point &operator=(const Point &p);
    Point &operator+=(const Point &p);
    Point &operator-=(const Point &p);
    Point &operator*=(const Point &p);
    Point &operator*=(const double &a);
    Point &operator/=(const Point &p);
    Point &operator/=(const double &a);
};

Point operator+(const Point &p1, const Point &p2);
Point operator-(const Point &p1, const Point &p2);
Point operator*(const Point &p, const double &a);
Point operator*(const double &a, const Point &p);
Point operator*(const Point &p1, const Point &p2);
Point operator/(const Point &p, const double &a);
Point operator/(const Point &p1, const Point &p2);

std::ostream &operator<<(std::ostream &stream, const Point& p);