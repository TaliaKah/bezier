#pragma once

#include "../include/point.hpp"

bool is_in_circle(Point point, Point center, float radius_center);
