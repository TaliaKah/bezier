#include <p6/p6.h>
#include "render/include/curve.hpp"
#include "render/include/event.hpp"
#include "render/include/control_point.hpp"
#include <vector>

int main()
{
    Window window;
    Curve bezier_order_2;
    bezier_order_2._type = curveType::bezier_order_2;
    window.context.update = [&] ()
    {        
        clear(window,{1.f, 1.f, 1.f, 0.75f});
            window.context.mouse_pressed = [&] (p6::MouseButton) 
            {
            Point mouse_pressed_position(window.context.mouse().x,window.context.mouse().y);
            Control_point control_point_mouse_pressed_position(mouse_pressed_position);
            add_point(bezier_order_2,control_point_mouse_pressed_position);
        };
        render(window, bezier_order_2);
    };
    start(window);
}