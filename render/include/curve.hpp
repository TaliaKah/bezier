#pragma once

#include <p6/p6.h>
#include <vector>
#include "../include/window.hpp"
#include "../include/stroke.hpp"
#include "../include/control_point.hpp"
#include "../../math/include/bezier.hpp"

enum class curveType{
    line,
    affine,
    bezier_order_2
};

struct Curve
{
    std::vector<Control_point> _points;
    curveType _type;
};

void add_point(Curve &curve, const Control_point &new_point_to_add);
void render(Window &window, const Curve &curve_to_render);