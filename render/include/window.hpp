#pragma once

#include <p6/p6.h>

struct Window{
    p6::Context context;
    p6::Color background;
};

void set_background_color(Window &window, const p6::Color &color);
void show_background_color(Window &window);

void clear(Window &window, const p6::Color &color);
void start(Window &window);