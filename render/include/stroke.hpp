#pragma once

#include <p6/p6.h>
#include "../include/window.hpp"

struct Stroke{
    p6::Color color;
    float weight;
};

void stroke_color(Window &win, const p6::Color &color);
void stroke_weight(Window &win, const float &weight);

void stroke_set(Window &win, Stroke &stroke);
