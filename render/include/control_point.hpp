#pragma once
#include "../../math/include/bezier.hpp"
#include "../include/window.hpp"
#include "../include/event.hpp"

#include <p6/p6.h>
#include <iostream>
#include <math.h>

class Control_point {

    p6::Color _color;
    p6::Radius _weight;
    Point _position;

    public:

    Control_point(const Point &point);
    ~Control_point()=default;

    Point get_position() const;

    void render(Window &window);
    bool mouse_is_on(Window &window);
};