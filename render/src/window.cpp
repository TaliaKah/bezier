#include "../include/window.hpp"

void set_background_color(Window &window, const p6::Color &color) 
{
    window.background = color;
}

void show_background_color(Window &window)
{
    window.context.background(window.background);
}

void clear (Window &window, const p6::Color &color)
{
    set_background_color(window, color);
    show_background_color(window);
}

void start(Window &window)
{
    window.context.start();
}