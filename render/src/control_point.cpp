#include "../include/control_point.hpp"
#include "../../math/include/is_in_a_shape.hpp"

Control_point::Control_point(const Point &point)
    : _position(point), _weight(0.02), _color(p6::Color{1.f, 0.f, 0.f, 0.5f})
{}

Point Control_point::get_position() const 
{
    return _position;
}

void Control_point::render(Window &window)
{
    p6::Center center = p6::Center(_position.get_x(), _position.get_y());
    window.context.stroke = _color;
    window.context.circle(center, _weight);
}

bool Control_point::mouse_is_on(Window &window)
{
    Point mouse_position(window.context.mouse().x, window.context.mouse().y);
    return (is_in_circle(mouse_position, _position, _weight.value)); 
}