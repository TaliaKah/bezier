#include "../include/stroke.hpp"

void stroke_color(Window &win, const p6::Color &color){
    win.context.stroke = color;
}

void stroke_weight(Window &win, const float &weight){
    win.context.stroke_weight = weight;
}
void stroke_set(Window &win,  Stroke &stroke){
    stroke_color(win,stroke.color);
    stroke_weight(win,stroke.weight);
}