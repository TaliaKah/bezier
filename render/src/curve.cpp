#include "../include/curve.hpp"

void add_point(Curve &curve, const Control_point &new_point_to_add)
{
    curve._points.push_back(new_point_to_add);
}

void draw_line(Window &window,  Stroke &stroke, const Point &start, const Point &end)
{
    stroke_set(window,stroke);
    window.context.line(glm::vec2(start.get_x(), start.get_y()),
                        glm::vec2(end.get_x(), end.get_y()));
}

void affine (Window &window, const p6::Color &color, const Point &B, const float &a)
{
    Point start(B);
    Point end(B);
    Stroke stroke;
    stroke.color = color;
    stroke.weight = 0.01f;
    for (int i = 0; i < 10; i++)
    {
        start = end;
        end = Point(i * .1, a * i * .1);
        draw_line(window, stroke, start, end);
    }
}

void bezier (Window &window, const p6::Color &color, const Point &A, const Point &B, const Point &C)
{
    Point start(0.f, 0.f);
    Point end(0.f, 0.f);
    Stroke stroke;
    stroke.color = color;
    stroke.weight = 0.01f;
    for (int i = 0; i < 10; i++)
    {
        start = end;
        end = Bezier_order_2(i * 0.1 , A , B , C);
        draw_line(window, stroke, start, end);
    }
}

void render_line(Window &window, const Curve &line)
{
    if (line._points.size()>1
    ){
        p6::Color color = {.2f, 1.f, 0.f, 0.75f};
        Stroke stroke;
        stroke.color = color;
        stroke.weight = 0.01f;
        for (size_t i = 1; i < line._points.size(); i++)
        {
            draw_line(window, stroke, 
                      line._points[i-1].get_position(),
                      line._points[i].get_position());
        }
    }
}

void render_affine(Window &window, const Curve &affine_curve)
{
    if (affine_curve._points.size()>0){
        p6::Color color = {.2f, 1.f, .1f, 0.75f};
        for (size_t i = 0; i < affine_curve._points.size(); i++)
        {
            affine(window, color, 
                   affine_curve._points[i].get_position(), 1.f);
        }
    }
}

void render_bezier_order_2(Window &window, const Curve &bezier_curve)
{
    if (bezier_curve._points.size()>2)
    {
        p6::Color color = {.2f, 0.f, .1f, 0.75f};
        for (size_t i = 2; i<bezier_curve._points.size();i++)
        {
            bezier(window, color, 
                   bezier_curve._points[i-2].get_position(),
                   bezier_curve._points[i-1].get_position(),
                   bezier_curve._points[i].get_position());
        }
    }
}

void render(Window &window, const Curve &curve_to_render)
{
    switch (curve_to_render._type) 
    {
        case curveType::line :
            render_line(window, curve_to_render);
            break;
        case curveType::affine :
            render_affine(window, curve_to_render);
            break;
        case curveType::bezier_order_2 :
            render_bezier_order_2(window, curve_to_render);
            break;
        default:
            break;
    }
}