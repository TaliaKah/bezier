#include "../include/event.hpp"

bool mouse_click(Window &window) 
{
    bool test = false;
    window.context.mouse_pressed = [&](p6::MouseButton) 
    {
        test = true;
    };
    return test;
}