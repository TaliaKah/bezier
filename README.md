# Bezier

## Description

The purpose of the project is to draw Bezier curves.
When launched you can click on a position with your mouse to make apear a control point. You have to create at least 3 point to draw your bezier curve. 
In main.cpp the type of curve drawn is bezier_order_2, you can change the type of curve by line or affine if you want. 
You can notice that control_point apears when your mouse is on, so you can enjoy the curve.

## Ideas of improvement

If I made the control point diseapears is it because of the idea that it could be cool to move or remove them by clicking. Also, it could be better if we can draw other more interssant curves as a bezier curve of order 3.

## How I did it

So, I have separate in two my code. On a hand a math directory for all mathematical functions where I use the standard library of cpp. On the other hand a render directory where I use my math functions and p6.
To make the code cleaner and better I have read Jules Fouchy's lesson (level 1 and 2, then some lessons of level 3 (std::vector, Minimize dependencies,  strongTypes,use librairies, range-based for loop, const, git submodules, assert, git pull requests))

## Difficulties

I had some difficulties to end the project.  I had accepted an update to my Linux mint and unfortunately there was a bug in the update. Therefore I can't draw a window with p6, so I made my last commits without knowing what will be on the window. 🤪
